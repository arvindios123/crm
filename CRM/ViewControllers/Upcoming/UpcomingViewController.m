//
//  UpcomingViewController.m
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "UpcomingViewController.h"
#import "NMGMoreView.h"
#import "NMGTopView.h"
#import "STBottomBar.h"
#import "UPCommingObject.h"
@interface UpcomingViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSInteger pagecount;
    IBOutlet UITableView *tblUpcoming;
}
@property (strong, nonatomic) NMGMoreView *leftMenu;
@property(nonatomic,retain)NMGTopView *topViewcontroller;
@property(nonatomic,retain)IBOutlet UIView *topBar;
@property(nonatomic,retain)STBottomBar *bottomBar;
@property(nonatomic,retain)IBOutlet UIView *view_Bottombar;
@property(nonatomic,retain)NSMutableArray *array_CommingData;
@end
@implementation tableCellUpcoming
@synthesize lblTime,lblDate,lblCountry,lblMatchNo,lblTeamName,lblStadiumName;
@end
@implementation UpcomingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    pagecount=0;
    self.array_CommingData =[[NSMutableArray alloc]init];
    tblUpcoming.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //top bar
    self.topViewcontroller = [[NMGTopView alloc]initWithController:self  andViewmenu:self.view];
    [self.topBar addSubview:self.topViewcontroller.view];
    
    //BOTTOM bar
    self.bottomBar=[[STBottomBar alloc]initWithController:self andVView:self.view andType:@""];
    [self.view_Bottombar addSubview:self.bottomBar.view];
    [self callToForgotService];
    // Do any additional setup after loading the view.
}
//--------------------------------------------------
#pragma mark -
#pragma mark---TABLEVIEW methods
//--------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.array_CommingData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
      UPCommingObject *object = [self.array_CommingData objectAtIndex:indexPath.row];
    static NSString *simpleTableIdentifier = @"tableCellUpcoming";
    tableCellUpcoming *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[tableCellUpcoming alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lblTeamName.text =[NSString stringWithFormat:@"%@ VS %@",object.TEAM1,object.TEAM2];
    cell.lblTime.text =object.GAME_TIME;
    cell.lblMatchNo.text =[NSString stringWithFormat:@"%@ %@",object.GAME_INFO,object.GAME_TYPE];
    cell.lblSeries.text =[NSString stringWithFormat:@"%@",object.SERIES_NAME];
    cell.lblDate.text =[NSString stringWithFormat:@"%@",object.GAME_DATE];
    cell.lblStadiumName.text =[NSString stringWithFormat:@"%@ %@",object.VENUE,object.CITY];
    cell.lblCountry.text =[NSString stringWithFormat:@"%@",object.COUNTRY];
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    WebViewController *create = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    //    [self.navigationController pushViewController:create animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callToForgotService
{
    
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
   
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/games.svc/game_schedulesv1",SERVERURLPATH]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:300.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSString *str =[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    int timestamps = [str intValue];
    str = [NSString stringWithFormat:@"%d",timestamps];
    double timeStamp = timestamps;
    NSTimeInterval timeInterval=timeStamp ;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    NSDictionary *paramsDictionary= @{
                                        @"PAGEINDEX":[NSString stringWithFormat:@"%ld",(long)pagecount],
                                        @"UPCOMING":@"1",
                                        @"GAME_TYPE":@"4",
                                        @"GAMEDATE":@"0"
                                        };
    
    NSDictionary *rootDic =@{@"GAME_SCHEDULE":paramsDictionary
                             
                             };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:rootDic options:0 error:&error];
    
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", @"CMazaa", @"cMazaA"];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setValue:@"" forHTTPHeaderField:@"REGISTRATIONID"];
    [request setValue:@"CMm0B!LE" forHTTPHeaderField:@"User-Agent"];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            NSError *error = nil;
             NSString *respString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"Responce = %@",respString);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil)
            {
               
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self checkResponse:json];
            });
        }
        else
        {
            NSLog(@"Error : %@",error.description);
        }
        
    }];
    
    [postDataTask resume];
    
    
    
    
//    NSString *str =[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
//    NSLog(@"%@",str);
//    ECSServiceClass * class = [[ECSServiceClass alloc]init];
//    [class setServiceMethod:GET];
//    [class setServiceURL:[NSString stringWithFormat:@"%@/games.svc/game_schedulesv1",SERVERURLPATH]];
//    [class addHeader:@"" forKey:@"REGISTRATIONID"];
//    [class addHeader:@"CMm0B!LE" forKey:@"User-Agent"];
//    NSDictionary *paramsDictionary= @{
//                                      @"PAGEINDEX":[NSString stringWithFormat:@"%ld",(long)pagecount],
//                                       @"UPCOMING":@"1",
//                                       @"GAME_TYPE":@"4",
//                                      @"GAMEDATE":@"1524463687285"
//                                      };
//
//
//
//
//    [class addParam:paramsDictionary.description  forKey:@"GAME_SCHEDULE"];
//
//    [class setCallback:@selector(callBackServiceGetUserObject:)];
//    [class setController:self];
//    [class runService];
}
- (void)checkResponse:(NSDictionary *)json
{
  
        NSArray *array_news = [json objectForKey:@"game_schedulesv1Result"];
        for(NSDictionary * dictionary in array_news)
        {
    
            UPCommingObject *newsObject=[UPCommingObject  instanceFromDictionary:dictionary];
            [self.array_CommingData addObject:newsObject];
    
    
    
    
        }
    [tblUpcoming reloadData];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        NSLog(@"Scroll End Called");
        pagecount++;
        [self callToForgotService];
        
    }
}
@end
