//
//  UpcomingViewController.h
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface tableCellUpcoming: UITableViewCell
@property (nonatomic,strong)  IBOutlet UILabel *lblTeamName,*lblTime,*lblMatchNo,*lblSeries,*lblDate,*lblStadiumName,*lblCountry;
@end
@interface UpcomingViewController : UIViewController

@end
