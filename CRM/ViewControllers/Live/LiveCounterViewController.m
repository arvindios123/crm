//
//  LiveCounterViewController.m
//  CRM
//
//  Created by apple on 18/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "LiveCounterViewController.h"
#import "NMGMoreView.h"
#import "NMGTopView.h"
#import "STBottomBar.h"
@interface LiveCounterViewController ()
{
     int count ;
     NSTimer *otpTimer;
}


// code push
@property(nonatomic,retain)IBOutlet UIImageView *img_team1;
@property(nonatomic,retain)IBOutlet UIImageView *img_team2;
@property(nonatomic,retain)IBOutlet UILabel *lbl_team1;
@property(nonatomic,retain)IBOutlet UILabel *lbl_team2;
@property(nonatomic,retain)IBOutlet UILabel *lbl_GameType;
@property(nonatomic,retain)IBOutlet UILabel *lbl_labi1;
@property(nonatomic,retain)IBOutlet UILabel *lbl_labi2;
@property(nonatomic,retain)IBOutlet UILabel *lbl_teamRate1;
@property(nonatomic,retain)IBOutlet UILabel *lbl_teamRate2;
@property(nonatomic,retain)IBOutlet UILabel *lbl_GameInfo;
@property(nonatomic,retain)IBOutlet UILabel *lbl_GameTime;
@property(nonatomic,retain)IBOutlet UILabel *lbl_Counter;

@property (strong, nonatomic) NMGMoreView *leftMenu;
@property(nonatomic,retain)NMGTopView *topViewcontroller;
@property(nonatomic,retain)IBOutlet UIView *topBar;
@property(nonatomic,retain)STBottomBar *bottomBar;
@property(nonatomic,retain)IBOutlet UIView *view_Bottombar;
@end

@implementation LiveCounterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //top bar
    self.topViewcontroller = [[NMGTopView alloc]initWithController:self  andViewmenu:self.view];
    [self.topBar addSubview:self.topViewcontroller.view];
    [self.img_team1 makeCircular];
    [self.img_team2 makeCircular];
    [self.img_team2 makeBorderWidth:[UIColor colorWithR:67 G:67 B:67]];
     [self.img_team1 makeBorderWidth:[UIColor colorWithR:67 G:67 B:67]];
    //BOTTOM bar
    self.bottomBar=[[STBottomBar alloc]initWithController:self andVView:self.view andType:@""];
    [self.view_Bottombar addSubview:self.bottomBar.view];
    [self callToLiveCommingService];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)callToForgotService
{
    
    ECSServiceClass * class = [[ECSServiceClass alloc]init];
    [class setServiceMethod:POST];
    [class setServiceURL:[NSString stringWithFormat:@"%@/games.svc/live_teams",SERVERURLPATH]];
    [class addHeader:@"" forKey:@"REGISTRATIONID"];
    [class addHeader:@"CMm0B!LE" forKey:@"User-Agent"];
    
    [class setCallback:@selector(callBackServiceGetUserObject:)];
    [class setController:self];
    [class runService];
}

-(void)callBackServiceGetUserObject:(ECSResponse *)response
{
    
    NSLog(@"%@",response.stringValue);
    NSDictionary *re_dic = [NSJSONSerialization JSONObjectWithData: response.data options: NSJSONReadingMutableContainers error: nil];
    NSDictionary *rootDictinary = [re_dic objectForKey:@"live_teamsResult"];
   
        [_img_team1 sd_setImageWithURL:[NSURL URLWithString:[[rootDictinary valueForKey:@"TEAM1_IMAGE"] stringByReplacingOccurrencesOfString:@"\\" withString:@"//"]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
    [_img_team2 sd_setImageWithURL:[NSURL URLWithString:[[rootDictinary valueForKey:@"TEAM2_IMAGE"] stringByReplacingOccurrencesOfString:@"\\" withString:@"//"]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
        
    _lbl_team1.text = [rootDictinary valueForKey:@"TEAM1"];
      _lbl_team2.text = [rootDictinary valueForKey:@"TEAM2"];
        
    
    [self callToLiveCommingService];
    
    //[self.tblLineFeed reloadData];
}

-(void)callToLiveCommingService
{
    
    ECSServiceClass * class = [[ECSServiceClass alloc]init];
    [class setServiceMethod:POST];
    [class setServiceURL:[NSString stringWithFormat:@"%@/games.svc/live_upcoming",SERVERURLPATH]];
    [class addHeader:@"" forKey:@"REGISTRATIONID"];
    [class addHeader:@"CMm0B!LE" forKey:@"User-Agent"];
    
    [class setCallback:@selector(callBackServiceGetLiveCommingService:)];
    [class setController:self];
    [class runService];
}

-(void)callBackServiceGetLiveCommingService:(ECSResponse *)response
{
    NSLog(@"%@",response.stringValue);
    NSDictionary *re_dic = [NSJSONSerialization JSONObjectWithData: response.data options: NSJSONReadingMutableContainers error: nil];
    NSDictionary *rootDictinary = [re_dic objectForKey:@"live_upcomingResult"];
    
    [_img_team1 sd_setImageWithURL:[NSURL URLWithString:[[rootDictinary valueForKey:@"TEAM1_IMAGE"] stringByReplacingOccurrencesOfString:@"\\" withString:@"//"]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    [_img_team2 sd_setImageWithURL:[NSURL URLWithString:[[rootDictinary valueForKey:@"TEAM2_IMAGE"] stringByReplacingOccurrencesOfString:@"\\" withString:@"//"]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    _lbl_team1.text = [rootDictinary valueForKey:@"TEAM1"];
    _lbl_team2.text = [rootDictinary valueForKey:@"TEAM2"];
    _lbl_labi1.text = [rootDictinary valueForKey:@"TEAM1_LAMBI"];
    _lbl_labi2.text = [rootDictinary valueForKey:@"TEAM2_LAMBI"];
    _lbl_teamRate1.text = [rootDictinary valueForKey:@"TEAM1_RATE"];
    _lbl_teamRate2.text = [rootDictinary valueForKey:@"TEAM2_RATE"];
   
    _lbl_GameType.text = [rootDictinary valueForKey:@"GAME_TYPE"];
    _lbl_GameInfo.text = [NSString stringWithFormat:@"%@ Match, %@",[rootDictinary valueForKey:@"GAME_INFO"],[rootDictinary valueForKey:@"GAME_TYPE"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    NSString *string = [formatter stringFromDate:[NSDate date]];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormater setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date1 = [dateFormater dateFromString:string];
    
//    2018-04-24 11:37:30 AM
//    2018-04-24 11:37:30 +0000
//    2018-04-24 06:07:30 +0000
//    2018-04-24 06:07:30 +0000
    
    double timeStamp = [[rootDictinary valueForKey:@"SERVER_DATETIME"] intValue];
    NSTimeInterval timeInterval=timeStamp ;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateformatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *dateString=[dateformatter stringFromDate:date];
    NSDateFormatter *dateFormaterrr = [[NSDateFormatter alloc] init];
    [dateFormaterrr setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormaterrr setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date2 = [dateFormaterrr dateFromString:dateString];
    NSTimeInterval secondsBetween = [date1 timeIntervalSinceDate:date2];
    
   NSInteger seconds = [date1 timeIntervalSinceDate:date2];
    
    
    count=secondsBetween;
    otpTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                target: self
                                              selector:@selector(countUp)
                                              userInfo: nil repeats:YES];
    
    
    
    
}
-(void)countUp
{
    if(count>=0)
    {
        NSString *house = [NSString stringWithFormat:@"%d",count / 3600];
        NSString *first = [NSString stringWithFormat:@"%d",(count/60) % 60];
        NSString *second = [NSString stringWithFormat:@"%d",count % 60];
        NSLog(@"%@:%@:%@",house,first,second);
        if(second.length == 1)
        {
            NSString *countString =[NSString stringWithFormat:@"%@:%@:0%@",house,first,second];
            NSLog(@"%@:%@:%@",house,first,second);
            self.lbl_Counter.text = countString;
        }
        else{
            NSString *countString =[NSString stringWithFormat:@"%@:%@:%@",house,first,second];
            NSLog(@"%@:%@",first,second);
            self.lbl_Counter.text = countString;
        }
        
      
        count--;
        
        
    }
    else{
       
    }
    
}


@end
