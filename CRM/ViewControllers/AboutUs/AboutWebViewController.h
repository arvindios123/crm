//
//  AboutWebViewController.h
//  CRM
//
//  Created by apple on 18/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutWebViewController : UIViewController
{
    IBOutlet UIWebView *webViewAboutUs;
    
}
@property(atomic,strong)NSString *strComeFrom;
@end
