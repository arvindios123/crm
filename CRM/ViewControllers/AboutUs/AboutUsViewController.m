//
//  AboutUsViewController.m
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "AboutUsViewController.h"
#import "NMGMoreView.h"
#import "NMGTopView.h"
#import "STBottomBar.h"
#import "AboutWebViewController.h"
@interface AboutUsViewController ()
{
    NSMutableArray *arrData;
    IBOutlet UITableView *tblAboutUs;
}
@property (strong, nonatomic) NMGMoreView *leftMenu;
@property(nonatomic,retain)NMGTopView *topViewcontroller;
@property(nonatomic,retain)IBOutlet UIView *topBar;
@property(nonatomic,retain)STBottomBar *bottomBar;
@property(nonatomic,retain)IBOutlet UIView *view_Bottombar;
@end
@implementation tableCellAboutUs
@synthesize lblname;
@end
@implementation AboutUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //top bar
    self.topViewcontroller = [[NMGTopView alloc]initWithController:self  andViewmenu:self.view];
    [self.topBar addSubview:self.topViewcontroller.view];
    
    //BOTTOM bar
    self.bottomBar=[[STBottomBar alloc]initWithController:self andVView:self.view andType:@""];
    [self.view_Bottombar addSubview:self.bottomBar.view];
    tblAboutUs.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    arrData=[[NSMutableArray alloc]init];
    [arrData addObject:@"About Us"];
    [arrData addObject:@"Contact Us"];
    [arrData addObject:@"Terms & Service"];
    [arrData addObject:@"Privacy Policy"];
    [arrData addObject:@"Refund and Cancellation Policy"];

    // Do any additional setup after loading the view.
}
//--------------------------------------------------
#pragma mark -
#pragma mark---IBACTION methods
//--------------------------------------------------
-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//--------------------------------------------------
#pragma mark -
#pragma mark---TABLEVIEW methods
//--------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    AcademicsObject *object = [self.arrResponse objectAtIndex:indexPath.row];
    static NSString *simpleTableIdentifier = @"tableCellAboutUs";
    tableCellAboutUs *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[tableCellAboutUs alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    cell.lblname.text=[arrData objectAtIndex:indexPath.row];
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AboutWebViewController *create = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutWebViewController"];
    if (indexPath.row==0)
    {
        create.strComeFrom=@"About";
    }
    else if (indexPath.row==1)
    {
        create.strComeFrom=@"Contact";
    }
    else if (indexPath.row==1)
    {
        create.strComeFrom=@"Terms";
    }
    else if (indexPath.row==1)
    {
        create.strComeFrom=@"Privacy";
    }
    else
    {
        create.strComeFrom=@"Refund";
    }

    [self.navigationController pushViewController:create animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
