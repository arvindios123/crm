//
//  AboutWebViewController.m
//  CRM
//
//  Created by apple on 18/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "AboutWebViewController.h"
#import "SingltonClass.h"
@interface AboutWebViewController ()<UIWebViewDelegate>
{
    NSString *urlString,*googlePage;
}
@end

@implementation AboutWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [SingltonClass startAnimation:self.view];
    webViewAboutUs.scalesPageToFit = YES;
    webViewAboutUs.contentMode = UIViewContentModeScaleAspectFit;
    if ([_strComeFrom isEqualToString:@"About"])
    {
        urlString = [NSString stringWithFormat:@"http://www.cricketmazza.com/about-us.php"];
    }
    else if ([_strComeFrom isEqualToString:@"Contact"])
    {
        urlString = @"http://www.cricketmazza.com/contact-us.php";
    }
    else if ([_strComeFrom isEqualToString:@"Terms"])
    {
        urlString = @"http://www.cricketmazza.com/terms.php";
    }
    else if ([_strComeFrom isEqualToString:@"Privacy"])
    {
       urlString = @"http://www.cricketmazza.com/privacy.php";
    }
    else
    {
        urlString = @"http://www.cricketmazza.com/refund.php";
    }
    
    [self loadUrlOnWebVioew:urlString];
    // Do any additional setup after loading the view.
}
//--------------------------------------------------
#pragma mark -
#pragma mark---IBACTION methods
//--------------------------------------------------
-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//--------------------------------------------------
#pragma mark -
#pragma mark---WEBVIEW DELEGATE methods
//--------------------------------------------------
-(void)loadUrlOnWebVioew:(NSString *)str_url
{
    NSURL *url = [NSURL URLWithString:str_url];
    NSError *error;
    googlePage = [NSString stringWithContentsOfURL:url
                                          encoding:NSASCIIStringEncoding
                                             error:&error];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webViewAboutUs setScalesPageToFit:YES];
    [webViewAboutUs loadRequest:request];
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [SingltonClass startAnimation:self.view];
    NSLog(@"start");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [SingltonClass stopAnoimation];
    NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
