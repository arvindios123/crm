//
//  SignUpViewController.m
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //[viewSignUp setViewBorderColor];
    [txtEmail setPadding];
    [txtMobile setPadding];
    [txtFullName setPadding];
    [txtPassword setPadding];
    [txtEmail setValue:[UIColor colorWithR:76 G:88 B:100]
            forKeyPath:@"_placeholderLabel.textColor"];
    [txtFullName setValue:[UIColor colorWithR:76 G:88 B:100]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtMobile setValue:[UIColor colorWithR:76 G:88 B:100]
            forKeyPath:@"_placeholderLabel.textColor"];
    [txtPassword setValue:[UIColor colorWithR:76 G:88 B:100]
               forKeyPath:@"_placeholderLabel.textColor"];
     
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
