//
//  SignUpViewController.h
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController
{
    IBOutlet UIView *viewSignUp;
    IBOutlet UITextField *txtEmail,*txtMobile,*txtFullName,*txtPassword;
}
@end
