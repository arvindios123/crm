//
//  MyAccountViewController.h
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountViewController : UIViewController
{
    IBOutlet UITextField *txtEmail,*txtPhone,*txtName;
    IBOutlet UILabel *lblSubscriptionPlan,*lblValidDate;
}
@end
