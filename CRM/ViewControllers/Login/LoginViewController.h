//
//  LoginViewController.h
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
///Users/apple/Desktop/CM/CRM/ViewControllers/Login/LoginViewController.h

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
{
    IBOutlet UIView *viewLogin;
    IBOutlet UITextField *txtEmail,*txtPassword;
}
@end
