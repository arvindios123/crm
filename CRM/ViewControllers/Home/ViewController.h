//
//  ViewController.h
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CRMHomeCounterCell : UITableViewCell
@property NSTimer *timer;
@property (nonatomic,strong)IBOutlet UILabel *lbl_counter;
@property (nonatomic,strong)IBOutlet UILabel *lbl_team;
@property (nonatomic,strong)IBOutlet UILabel *lbl_DateDay;
@property (nonatomic,strong)IBOutlet UILabel *lbl_MatchType;
@end

@interface CRMHomeTableCell : UITableViewCell
@property (nonatomic,strong)  IBOutlet UIImageView *imgViewC3Banner;
@property (nonatomic,strong)  IBOutlet UILabel *lblC2TeamName,*lblC2TimeCounter,*lblC2Date,*lblC2MatchNumber,*lblC4MatchTitle,*lblC4Date;
@property (nonatomic,strong)  IBOutlet UIButton *btnC3ViewAll,*btnC4ViewMore;
@property (nonatomic,strong)  IBOutlet UIView *view_Scroll;
@end
@interface ViewController : UIViewController
{
    IBOutlet UITableView *tblViewHome;
}

@end

