//
//  ViewController.m
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "ViewController.h"
#import "NMGMoreView.h"
#import "NMGTopView.h"
#import "STBottomBar.h"
#import "NewsObject.h"
#import "MatchObject.h"
#import "UPCommingObject.h"

@interface ViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
UIScrollView *someScrollView,*sizeScroll,*colourScroll;
UIPageControl *pageControl;
NSString *currentPageIndexString,*numberofPages,*size,*sizetitle;
     BOOL indexRelodValue;
}

@property(nonatomic,retain)IBOutlet UITableView *tbl_news;
@property (strong, nonatomic) NMGMoreView *leftMenu;
@property(nonatomic,retain)NMGTopView *topViewcontroller;
@property(nonatomic,retain)IBOutlet UIView *topBar;
@property(nonatomic,retain)STBottomBar *bottomBar;
@property(nonatomic,retain)IBOutlet UIView *view_Bottombar;
@property(nonatomic,retain)NSMutableArray *array_newsData;
@property(nonatomic,retain)NSMutableArray *array_match;
@property(nonatomic,retain)NSMutableArray *array_nextGame;
@end
@implementation CRMHomeCounterCell
@synthesize lbl_counter;
@end
@implementation CRMHomeTableCell
@synthesize lblC2TimeCounter;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    NSString *string = [formatter stringFromDate:[NSDate date]];
    NSDate *date = [formatter dateFromString:string];
    
    
    
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    
    double timeStamp = 1524546104330;
    NSTimeInterval timeInterval=timeStamp ;
    NSDate *date1 = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    [dateformatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *dateString=[dateformatter stringFromDate:date];
   
   
    
    indexRelodValue =YES;
    
    //top bar
    self.topViewcontroller = [[NMGTopView alloc]initWithController:self  andViewmenu:self.view];
    [self.topBar addSubview:self.topViewcontroller.view];
    
    //BOTTOM bar
    self.bottomBar=[[STBottomBar alloc]initWithController:self andVView:self.view andType:@""];
    [self.view_Bottombar addSubview:self.bottomBar.view];
    
    
    [self callToForgotService];
    // Do any additional setup after loading the view, typically from a nib.
}
//---------------------------------------------------------------

#pragma mark
#pragma mark Tableview Delegates

//---------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 1;
    }
    else if (section==1)
    {
    return 1;
    }
    else if (section==2)
    {
        return 1;
    }
    else
    {
        return self.array_newsData.count;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return 166.0f;
        
    }
    else  if (indexPath.section==1)
    {
        return 154.0f;
    }
    else  if (indexPath.section==2)
    {
        return 212.0f;
        
    }else
    {
        return 85.0f;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *CellIdentifier;
    CRMHomeTableCell *cell;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    switch (indexPath.section)
    {
        case 0:
        {
            CellIdentifier = @"Cell1";
            cell= [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
            if (cell==nil)
            {
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            }
            if(indexRelodValue == YES)
            {
                cell.view_Scroll.backgroundColor = [UIColor clearColor];
            someScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,5, [UIScreen mainScreen].bounds.size.width,cell.view_Scroll.frame.size.height)];
            someScrollView.delegate=self;
            someScrollView.pagingEnabled = YES;
            someScrollView.backgroundColor = [UIColor clearColor];
            someScrollView.showsHorizontalScrollIndicator =NO;
            someScrollView.showsVerticalScrollIndicator =NO;
            
            someScrollView.contentSize = CGSizeMake(self.array_match.count * [UIScreen mainScreen].bounds.size.width, someScrollView.frame.size.height);
            [cell.view_Scroll addSubview:someScrollView];
            someScrollView.backgroundColor = [UIColor clearColor];
            
            for(int i=0;i<self.array_match.count;i++)
            {
                MatchObject *matchObject = [self.array_match objectAtIndex:i];
                
                UIView *view_match = [[UIImageView alloc] initWithFrame:CGRectMake(i * someScrollView.frame.size.width + 5,
                                                                                        0,
                                                                                        someScrollView.frame.size.width-10 ,
                                                                                        130.0f)];
                UILabel *view_Opecity = [[UILabel alloc]initWithFrame:CGRectMake(5,0,view_match.frame.size.width-20,view_match.frame.size.height-5)];
                
                [view_Opecity setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.1]];
                [view_Opecity setCornerRadius];
               
                [view_match addSubview:view_Opecity];
                UILabel *seprateLine = [[UILabel alloc]initWithFrame:CGRectMake(0,0,5,view_match.frame.size.height)];
                seprateLine.backgroundColor = [UIColor colorWithR:0 G:176 B:83];
                
                 [view_match addSubview:seprateLine];
                
                
                
              //  view_match.backgroundColor = [UIColor clearColor];
                
                
                
                UILabel *lbl_matchTeam = [[UILabel alloc]initWithFrame:CGRectMake(15,2,view_match.frame.size.width/2-8,80)];
            
                lbl_matchTeam.numberOfLines = 3;
                lbl_matchTeam.baselineAdjustment = YES;
                lbl_matchTeam.text = [NSString stringWithFormat:@"%@ VS %@",matchObject.TEAM1,matchObject.TEAM2];
                lbl_matchTeam.clipsToBounds = YES;
                lbl_matchTeam.font =[UIFont boldSystemFontOfSize:17.0];;
                lbl_matchTeam.backgroundColor = [UIColor clearColor];
                lbl_matchTeam.textColor = [UIColor whiteColor];
                lbl_matchTeam.textAlignment = NSTextAlignmentLeft;
                [lbl_matchTeam addConstraint:[NSLayoutConstraint constraintWithItem:lbl_matchTeam
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute: NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1
                                                                   constant:21]];
                [view_match addSubview:lbl_matchTeam];
                UILabel *lbl_TimeDate = [[UILabel alloc]initWithFrame:CGRectMake(15,80,view_match.frame.size.width/2-8,15)];
                
                lbl_TimeDate.numberOfLines = 3;
                lbl_TimeDate.baselineAdjustment = YES;
                lbl_TimeDate.text = [ECSDate getTimeFromTimeStemp:matchObject.GAME_TIME];
                lbl_TimeDate.clipsToBounds = YES;
                lbl_TimeDate.backgroundColor = [UIColor clearColor];
                lbl_TimeDate.textColor = [UIColor whiteColor];
                lbl_TimeDate.textAlignment = NSTextAlignmentLeft;
                UILabel *lbl_dateType = [[UILabel alloc]initWithFrame:CGRectMake(15,100,view_match.frame.size.width/2-8,20)];
                
                lbl_dateType.numberOfLines = 3;
                lbl_dateType.baselineAdjustment = YES;
                lbl_dateType.text =[NSString stringWithFormat:@"%@ Match, %@",matchObject.GAME_INFO,matchObject.GAME_TYPE];
                lbl_dateType.clipsToBounds = YES;
                lbl_dateType.font =[UIFont systemFontOfSize:13];
                lbl_dateType.backgroundColor = [UIColor clearColor];
                lbl_dateType.textColor = [UIColor brownColor];
                lbl_dateType.textAlignment = NSTextAlignmentLeft;
                
                
                [view_match addSubview:lbl_matchTeam];
                [view_match addSubview:lbl_dateType];
                 [view_match addSubview:lbl_TimeDate];
                
  
                
                UILabel *lbl_Date = [[UILabel alloc]initWithFrame:CGRectMake(view_match.frame.size.width/2,5,150,30)];
                
                lbl_Date.numberOfLines =1;
                lbl_Date.baselineAdjustment = YES;
                lbl_Date.text = @"25 Apr,2018";
                lbl_Date.clipsToBounds = YES;
                lbl_Date.backgroundColor = [UIColor clearColor];
                lbl_Date.textColor = [UIColor whiteColor];
                lbl_Date.textAlignment = NSTextAlignmentRight;
               
                UILabel *lbl_Stadium = [[UILabel alloc]initWithFrame:CGRectMake(view_match.frame.size.width/2-8,50,150,45)];
                
                lbl_Stadium.numberOfLines = 3;
                lbl_Stadium.baselineAdjustment = YES;
                lbl_Stadium.text =[NSString stringWithFormat:@"%@ %@", matchObject.VENUE, matchObject.CITY];
                lbl_Stadium.clipsToBounds = YES;
                lbl_Stadium.font =[UIFont systemFontOfSize:13];
                lbl_Stadium.backgroundColor = [UIColor clearColor];
                lbl_Stadium.textColor = [UIColor whiteColor];
                lbl_Stadium.textAlignment = NSTextAlignmentRight;
                UILabel *lbl_Contary = [[UILabel alloc]initWithFrame:CGRectMake(view_match.frame.size.width/2-8,85,150,25)];
                
                lbl_Contary.numberOfLines = 3;
                lbl_Contary.baselineAdjustment = YES;
                lbl_Contary.text = matchObject.COUNTRY;
                lbl_Contary.clipsToBounds = YES;
                lbl_Contary.font =[UIFont systemFontOfSize:13];
                lbl_Contary.backgroundColor = [UIColor clearColor];
                lbl_Contary.textColor = [UIColor brownColor];
                lbl_Contary.textAlignment = NSTextAlignmentRight;
                
                
                [view_match addSubview:lbl_Date];
                [view_match addSubview:lbl_Stadium];
                [view_match addSubview:lbl_Contary];
                
                
                
                
                
                
                [someScrollView addSubview:view_match];
                
                
                
                
            }
            if(self.array_match.count>1)
            {
               [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(autoStartSlide:) userInfo:nil repeats:YES];
            }
            
            // page control
            
            UIView *viewPage = [[UIView alloc]initWithFrame:CGRectMake(0,cell.contentView.frame.size.height-30,cell.contentView.frame.size.width,0)];
            
            viewPage.backgroundColor = [UIColor whiteColor];
            
            
            [cell.contentView addSubview:viewPage];
            
            pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(0,13, [UIScreen mainScreen].bounds.size.width,0)];
            
            pageControl.backgroundColor = [UIColor yellowColor];
            pageControl.numberOfPages=self.array_match.count;
            pageControl.currentPage = 0;  //new
           // currentPageIndexString = [NSString stringWithFormat:@"%ld",(long)0]; //new
            [viewPage addSubview:pageControl];
            
            
            if ([pageControl respondsToSelector:@selector(setPageIndicatorTintColor:)]) {
                
                pageControl.currentPageIndicatorTintColor = [UIColor colorWithR:242 G:112 B:49];
                
                pageControl.pageIndicatorTintColor = [UIColor grayColor];
            }
            
            CGRect frame = someScrollView.frame;
           // frame.origin.x = frame.size.width * [[NSString stringWithFormat:@"%@",numberofPages] integerValue];
            frame.origin.y = 0;
            [someScrollView scrollRectToVisible:frame animated:NO];
            //pageControl.currentPage = [[NSString stringWithFormat:@"%@",numberofPages] integerValue];
          //  indexRelodValue = NO;
            
            
            indexRelodValue = NO;
            cell.contentView.backgroundColor = [UIColor clearColor];
            return cell;
            }
           
        }
            break;
            
        case 1:
        {
            CellIdentifier = @"CellCounter";
            CRMHomeCounterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[CRMHomeCounterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            UPCommingObject *objectNext =[self.array_nextGame objectAtIndex:indexPath.row];
            
            
           
            cell= [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
            
             cell.contentView.backgroundColor = [UIColor clearColor];
          NSTimer *setIme  = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(comeDownData:) userInfo:nil repeats:YES];
        //setIme.tag
            cell.lbl_counter.text =@"hjsdjhf";
            cell.lbl_team.text = [NSString stringWithFormat:@"%@ VS %@",objectNext.TEAM1,objectNext.TEAM2];
            cell.lbl_MatchType.text = [NSString stringWithFormat:@"%@ Match, %@",objectNext.GAME_INFO,objectNext.GAME_TYPE];
     
            
            
            
            return cell;
        }
            break;
            
        case 2:
        {
            CellIdentifier = @"Cell3";
            cell= [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
            cell.contentView.backgroundColor = [UIColor clearColor];
            NewsObject *newsObject = [self.array_newsData objectAtIndex:indexPath.row];
            [cell.imgViewC3Banner sd_setImageWithURL:[NSURL URLWithString:[newsObject.imageurl stringByReplacingOccurrencesOfString:@"\\" withString:@"//"]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
            
            return cell;
            
            
            
            
            
        }
            break;
        case 3:
        {
             NewsObject *newsObject = [self.array_newsData objectAtIndex:indexPath.row];
            CellIdentifier = @"Cell4";
            cell= [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.lblC4MatchTitle.text =newsObject.title;
            cell.lblC4Date.text =[ECSDate getDateFromTimeStemp:newsObject.postDate];
            
            return cell;
            
        }
            break;
       
        default:
            break;
            
            
            
            
    }
    return cell;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)callToForgotService
{
    
    ECSServiceClass * class = [[ECSServiceClass alloc]init];
    [class setServiceMethod:POST];
    [class setServiceURL:[NSString stringWithFormat:@"%@/games.svc/home_screen",SERVERURLPATH]];
    [class addHeader:@"" forKey:@"REGISTRATIONID"];
    [class addHeader:@"CMm0B!LE" forKey:@"User-Agent"];
    [class setCallback:@selector(callBackServiceGetUserObject:)];
    [class setController:self];
    [class runService];
}

-(void)callBackServiceGetUserObject:(ECSResponse *)response
{
    self.array_nextGame =[[NSMutableArray alloc]init];
    self.array_match =[[NSMutableArray alloc]init];
    self.array_newsData =[[NSMutableArray alloc]init];
     NSDictionary *re_dic = [NSJSONSerialization JSONObjectWithData: response.data options: NSJSONReadingMutableContainers error: nil];
    NSDictionary *rootDictinary = [re_dic objectForKey:@"home_screenResult"];
    NSArray *array_news = [rootDictinary objectForKey:@"NEWS"];
    for(NSDictionary * dictionary in array_news)
    {
        
        NewsObject *newsObject=[NewsObject  instanceFromDictionary:dictionary];
        [self.array_newsData addObject:newsObject];
        
        
        
        
    }
    NSArray *array_match = [rootDictinary objectForKey:@"GAMES"];
    for(NSDictionary * dictionary in array_match)
    {
         indexRelodValue = YES;
        MatchObject *matchObject=[MatchObject  instanceFromDictionary:dictionary];
        [self.array_match addObject:matchObject];
        
        
        
        
    }
    NSArray *array_Next = [rootDictinary objectForKey:@"NEXT_GAMES"];
    for(NSDictionary * dictionary in array_Next)
    {
       // indexRelodValue = YES;
        UPCommingObject *matchObject=[UPCommingObject  instanceFromDictionary:dictionary];
        [self.array_nextGame addObject:matchObject];
        
        
        
        
    }
    
    [tblViewHome reloadData];
}

- (void)comeDownData:(id)sender
{
    NSTimer *tmer =(NSTimer *)sender;
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:0 inSection:1];
    //Type cast it to CustomCell
    CRMHomeCounterCell *cell = (CRMHomeCounterCell*)[self.tbl_news cellForRowAtIndexPath:myIP];
    
    //CRMHomeCounterCell *cell = (CRMHomeCounterCell *)[self.tbl_news cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
    
   // cell.lbl_counter.text = [NSString stringWithFormat:@"hgdsjfkg"];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger scrollIndex = (NSInteger)(someScrollView.contentOffset.x / someScrollView.frame.size.width);
    pageControl.currentPage = scrollIndex;
    currentPageIndexString = [NSString stringWithFormat:@"%ld",(long)pageControl.currentPage];
}
- (void)autoStartSlide:(id)sender {
    if(pageControl.numberOfPages > pageControl.currentPage+1) {
        [UIView animateWithDuration:0.8 animations:^{
            someScrollView.contentOffset = CGPointMake(someScrollView.contentOffset.x + someScrollView.frame.size.width, 0);
        } completion:^(BOOL finished) {
            
        }];
    }
    else {
        someScrollView.contentOffset = CGPointMake(someScrollView.contentOffset.x - someScrollView.frame.size.width, 0);
        [UIView animateWithDuration:0.8 animations:^{
            someScrollView.contentOffset = CGPointMake(someScrollView.contentOffset.x + someScrollView.frame.size.width, 0);
        } completion:^(BOOL finished) {
            someScrollView.contentOffset = CGPointMake(0, 0) ;
        }];
    }
    
}

@end
