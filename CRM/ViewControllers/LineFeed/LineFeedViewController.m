//
//  LineFeedViewController.m
//  CRM
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "LineFeedViewController.h"
#import "NMGMoreView.h"
#import "NMGTopView.h"
#import "STBottomBar.h"
#import "NewsObject.h"
@interface LineFeedViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    
        NSInteger pagecount;
        
  
    
}
@property(nonatomic,retain)IBOutlet UITableView *tblLineFeed;
@property(nonatomic,retain)NSMutableArray *array_FeedBack;
@property (strong, nonatomic) NMGMoreView *leftMenu;
@property(nonatomic,retain)NMGTopView *topViewcontroller;
@property(nonatomic,retain)IBOutlet UIView *topBar;
@property(nonatomic,retain)STBottomBar *bottomBar;
@property(nonatomic,retain)IBOutlet UIView *view_Bottombar;
@end
@implementation tableCellLineFeed
@synthesize lblname,lblTime;
@end
@implementation LineFeedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    pagecount=0;
    self.array_FeedBack = [[NSMutableArray alloc]init];
   // tblLineFeed.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //top bar
    self.tblLineFeed.rowHeight = UITableViewAutomaticDimension;
    self.tblLineFeed.estimatedRowHeight = 300.0;
    self.topViewcontroller = [[NMGTopView alloc]initWithController:self  andViewmenu:self.view];
    [self.topBar addSubview:self.topViewcontroller.view];
    
    //BOTTOM bar
    self.bottomBar=[[STBottomBar alloc]initWithController:self andVView:self.view andType:@""];
    [self.view_Bottombar addSubview:self.bottomBar.view];
    
    [self callToForgotService];
    // Do any additional setup after loading the view.
}
//--------------------------------------------------
#pragma mark -
#pragma mark---TABLEVIEW methods
//--------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.array_FeedBack.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
      NewsObject *object = [self.array_FeedBack objectAtIndex:indexPath.row];
    static NSString *simpleTableIdentifier = @"tableCellLineFeed";
    tableCellLineFeed *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[tableCellLineFeed alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[object.FEEDS dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    //      cell.lblname.text=object.FEEDS;
    cell.lblname.attributedText = attrStr;
    cell.lblname.textColor=[UIColor whiteColor];
    cell.lblTime.text = [ECSDate  getTimeFromTimeStemp:object.FEED_DATE];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    WebViewController *create = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    //    [self.navigationController pushViewController:create animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callToForgotService
{
    
    ECSServiceClass * class = [[ECSServiceClass alloc]init];
    [class setServiceMethod:GET];
    [class setServiceURL:[NSString stringWithFormat:@"%@/games.svc/live_feedsv1",SERVERURLPATH]];
    [class addHeader:@"" forKey:@"REGISTRATIONID"];
    [class addHeader:@"CMm0B!LE" forKey:@"User-Agent"];
    [class addParam:[NSString stringWithFormat:@"%ld",(long)pagecount] forKey:@"PAGEINDEX"];
    [class setCallback:@selector(callBackServiceGetUserObject:)];
    [class setController:self];
    [class runService];
}

-(void)callBackServiceGetUserObject:(ECSResponse *)response
{
    
    NSLog(@"%@",response.stringValue);
    NSDictionary *re_dic = [NSJSONSerialization JSONObjectWithData: response.data options: NSJSONReadingMutableContainers error: nil];
     NSDictionary *rootDictinary = [re_dic objectForKey:@"live_feedsv1Result"];
    NSArray *array_news = [rootDictinary objectForKey:@"FEEDS"];
    for(NSDictionary * dictionary in array_news)
    {
        
        NewsObject *newsObject=[NewsObject  instanceFromDictionary:dictionary];
        [self.array_FeedBack addObject:newsObject];
        
        
        
        
    }
    
   
       [self.tblLineFeed reloadData];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        NSLog(@"Scroll End Called");
        pagecount++;
        [self callToForgotService];
        
    }
}

@end
