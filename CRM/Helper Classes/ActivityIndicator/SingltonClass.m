//
//  SingltonClass.m
//  Activity
//
//  Created by Sterco Digitex on 28/03/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "SingltonClass.h"

@implementation SingltonClass
static MONActivityIndicatorView *singletonObject = nil;

+ (MONActivityIndicatorView *)sharedSampleSingletonClass
{
    if (! singletonObject) {
        
        singletonObject = [[MONActivityIndicatorView alloc] init];
    }
    return singletonObject;
}

- (id)init
{
    if (! singletonObject) {
        
        singletonObject = [super init];
        
    }
    return singletonObject;
}
+(void)startAnimation:(UIView *)view;
{
    
    
    
    [view addSubview:SingltonClass.sharedSampleSingletonClass];
    [SingltonClass.sharedSampleSingletonClass startAnimating];
   
   

    
}
+(void)stopAnoimation
{
    
    [SingltonClass.sharedSampleSingletonClass stopAnimating];
    
}



@end
