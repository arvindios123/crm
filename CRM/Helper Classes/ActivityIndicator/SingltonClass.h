//
//  SingltonClass.h
//  Activity
//
//  Created by Sterco Digitex on 28/03/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MONActivityIndicatorView.h>
@interface SingltonClass : NSObject
+(MONActivityIndicatorView *)sharedSampleSingletonClass;
+(void)startAnimation:(UIView *)view;
+(void)stopAnoimation;
@end
