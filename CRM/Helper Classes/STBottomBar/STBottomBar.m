//
//  STBottomBar.m
//  Monash
//
//  Created by Sterco Digitex on 04/09/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "STBottomBar.h"
#import "RecentViewController.h"
#import "UpcomingViewController.h"
#import "LiveCounterViewController.h"
#import "ViewController.h"
#import "LineFeedViewController.h"
@interface STBottomBar ()
{
     UIStoryboard *mainStoryboard;
}
@property(nonatomic,retain)UIViewController *controller;
@property(nonatomic,retain)UIView *views;
-(IBAction)click_ToHome:(id)sender;
-(IBAction)click_ToLineFeed:(id)sender;
-(IBAction)click_ToLive:(id)sender;
-(IBAction)click_ToRecent:(id)sender;
-(IBAction)click_ToUpcoming:(id)sender;
@end





@implementation STBottomBar
-(id)initWithController:(UIViewController *)controller andVView:(UIView *)view andType:(NSString *)type

{
    self.controller=controller;
    self.views=view;
   
    self.view.layer.shadowOpacity = 0.5;
    self.view.layer.shadowOffset = CGSizeMake(0, 0);
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self=[self initWithNibName:@"STBottomBar" bundle:nil];
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
   self.view.frame=CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 60);
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self UnselectedButton];
    if ([self.controller isKindOfClass:[ViewController class]])
    {
        [btnHome setTitleColor:[UIColor colorWithR:1 G:176 B:83] forState:UIControlStateNormal];

        imgHome.image=[UIImage imageNamed:@"homeSelected.png"];
        
    }
    else if([self.controller isKindOfClass:[LineFeedViewController class]])
    {
        [btnLineFeed setTitleColor:[UIColor colorWithR:1 G:176 B:83] forState:UIControlStateNormal];
       imgLineFeed.image=[UIImage imageNamed:@"lineFeedSelected.png"];
    }
    else if([self.controller isKindOfClass:[LiveCounterViewController class]])
    {
        [btnLive setTitleColor:[UIColor colorWithR:1 G:176 B:83] forState:UIControlStateNormal];
        imgLive.image=[UIImage imageNamed:@"liveSelected.png"];
    }
    else if([self.controller isKindOfClass:[RecentViewController class]])
    {
        [btnRecent setTitleColor:[UIColor colorWithR:1 G:176 B:83] forState:UIControlStateNormal];
       imgRecent.image=[UIImage imageNamed:@"recentSelected.png"];
    }
    else if([self.controller isKindOfClass:[UpcomingViewController class]])
    {
        [btnUpcoming setTitleColor:[UIColor colorWithR:1 G:176 B:83] forState:UIControlStateNormal];
        imgUpcoming.image=[UIImage imageNamed:@"upcomingSelected.png"];
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)UnselectedButton
{
        imgHome.image=[UIImage imageNamed:@"homeUnselected.png"];
        imgLineFeed.image=[UIImage imageNamed:@"lineFeedUnselected.png"];
        imgLive.image=[UIImage imageNamed:@"liveUnselected.png"];
        imgRecent.image=[UIImage imageNamed:@"recentUnselected.png"];
        imgUpcoming.image=[UIImage imageNamed:@"upcomingUnselected.png"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)click_ToHome:(id)sender
{
        ViewController *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.controller.navigationController pushViewController:home animated:NO];

}
-(IBAction)click_ToLineFeed:(id)sender
{
   

    LineFeedViewController *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"LineFeedViewController"];
    [self.controller.navigationController pushViewController:home animated:NO];

    


}
-(IBAction)click_ToLive:(id)sender
{
    
 
    LiveCounterViewController *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"LiveCounterViewController"];

    [self.controller.navigationController pushViewController:home animated:NO];


}
-(IBAction)click_ToRecent:(id)sender
{
  
 

        RecentViewController *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecentViewController"];

        [self.controller.navigationController pushViewController:home animated:NO];
    

}
-(IBAction)click_ToUpcoming:(id)sender
{    
       UpcomingViewController *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"UpcomingViewController"];
        [self.controller.navigationController pushViewController:home animated:NO];
   
    
}

@end
