//
//  STBottomBar.h
//  Monash
//
//  Created by Sterco Digitex on 04/09/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STBottomBar : UIViewController
{
    IBOutlet UIImageView *imgLive,*imgLineFeed,*imgUpcoming,*imgHome,*imgRecent;
    IBOutlet UIButton *btnLive,*btnLineFeed,*btnUpcoming,*btnHome,*btnRecent;
}
-(id)initWithController:(UIViewController *)controller andVView:(UIView *)view andType:(NSString *)type;
@end
