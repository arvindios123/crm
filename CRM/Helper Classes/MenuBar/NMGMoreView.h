//
//  NMGMoreView.h
//  MeDoFi
//
//  Created by ARVIND PANDEY on 5/12/16.
//  Copyright © 2016 Arvind Pandey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMGMoreView : UIViewController
{
    IBOutlet UILabel *lblEmail,*lblName;
    IBOutlet UIImageView *imagviewProfile;
    IBOutlet UIView *signInView;
}
-(id)initWithController:(UIViewController *)controller;
@property(nonatomic,retain)IBOutlet UIView *animateView;
@property(nonatomic,retain)UIViewController *controller;
@end
