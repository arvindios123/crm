//
//  NMGMoreView.m
//  MeDoFi
//
//  Created by ARVIND PANDEY on 5/12/16.
//  Copyright © 2016 Arvind Pandey. All rights reserved.
//

#import "NMGMoreView.h"
#import "LoginViewController.h"
#import "AboutUsViewController.h"
//#import "WebViewController.h"
//#import "AcademicsViewController.h"
//#import "AboutDITViewController.h"
//#import "GalleryViewController.h"
//#import "NewsEventsViewController.h"
//#import "ContactUsViewController.h"


#define YOUR_APP_STORE_ID 1044391056 //Change this one to your ID
@interface NMGMoreView ()
{
   
    UIStoryboard *mainStoryboard;
    NSInteger rowHight;
    IBOutlet UIWebView *webMenu;
}

@property(nonatomic,retain)NSMutableArray *arraydata,*arrayImages;
@property(nonatomic,retain)IBOutlet UITableView *tblView;
@property(nonatomic,retain)IBOutlet UIButton *btn_sign;
@property(nonatomic,retain)IBOutlet UIButton *btn_signUp;
@property(nonatomic,retain)IBOutlet UIImageView *logoImage;
-(IBAction)click_toBack:(id)sender;

@property BOOL type;


@end

@implementation NMGMoreView
-(id)initWithController:(UIViewController *)controller
{
  /// arvind
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self=[super initWithNibName:@"NMGMoreView" bundle:nil];
    self.controller=controller;
   
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.arraydata=[[NSMutableArray alloc]init];
    self.arrayImages=[[NSMutableArray alloc]init];
    
    [self.arraydata addObject:@"My Account"];
    [self.arraydata addObject:@"Subscription Plan"];
    [self.arraydata addObject:@"Share with Friends"];
    [self.arraydata addObject:@"About Us"];
    
    [self.arrayImages addObject:@"account.png"];
    [self.arrayImages addObject:@"subscription.png"];
    [self.arrayImages addObject:@"share.png"];
    [self.arrayImages addObject:@"logo.png"];
  
    _tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
      self.animateView.frame=CGRectMake(0, 0, self.animateView.frame.size.width, self.animateView.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return [self.arraydata count];

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    UILabel *cellLabel;
    UIImageView *img;
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    cell.backgroundColor=[UIColor clearColor];
    
    img= [[UIImageView alloc] initWithFrame:CGRectMake(40, cell.frame.size.height/2-13, 22,22)];
    cellLabel= [[UILabel alloc] initWithFrame:CGRectMake(75, cell.frame.size.height/2-15, 200,30)];
    
    img.image=[UIImage imageNamed:[_arrayImages objectAtIndex:indexPath.row]];
    img.contentMode = UIViewContentModeScaleAspectFit;
    
    cellLabel.text=[self.arraydata objectAtIndex:indexPath.row];
    cellLabel.textColor=[UIColor whiteColor];
    [cellLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:14]];
    [cellLabel setBackgroundColor:[UIColor clearColor]];
    
    

    
    [cell addSubview:cellLabel];
    [cell addSubview:img];
          
    
      return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [self.view removeFromSuperview];
    if (indexPath.row==0)
    {
        //LoginViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
       // [self.controller.navigationController pushViewController:create animated:YES];
    }
//    else if (indexPath.row==1)
//    {
//        ProgramsViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProgramsViewController"];
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
    else if (indexPath.row==2)
    {
        NSURL *urlToShare = [NSURL URLWithString:@"https://itunes.apple.com/in/app/cricket-mazza/id1218833601?mt=8"];
        
        NSString *textToShare = @"Cricket Mazza";
        
        NSArray *objectsToShare = @[textToShare, urlToShare];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        
        
        [self.controller presentViewController:activityVC animated:YES completion:nil];
    }
    else if (indexPath.row==3)
    {
        AboutUsViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
        [self.controller.navigationController pushViewController:create animated:YES];
    }
//    else if (indexPath.row==4)
//    {
//        WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//        create.strComeFrom=@"Infra";
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//    else if (indexPath.row==5)
//    {
//        WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//        create.strComeFrom=@"Acc";
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//    else if (indexPath.row==6)
//    {
//        WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//        create.strComeFrom=@"Sports";
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//    else if (indexPath.row==7)
//    {
//        WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//        create.strComeFrom=@"Ranking";
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//    else if (indexPath.row==8)
//    {
//        AboutDITViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"AboutDITViewController"];
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//    else if (indexPath.row==9)
//    {
//        GalleryViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"GalleryViewController"];
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//    else if (indexPath.row==10)
//    {
//        NewsEventsViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"NewsEventsViewController"];
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//    else
//    {
//        ContactUsViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
//        create.strComeFrom=@"Menu";
//        [self.controller.navigationController pushViewController:create animated:YES];
//    }
//
}

-(IBAction)click_toBack:(id)sender
{
       [UIView animateWithDuration:0.8 animations:^void
        {
            
            
        }];
       CATransition *transition = [CATransition animation];
       transition.duration = 0.5;
       transition.type = kCATransitionPush;
          transition.subtype = kCATransitionFromRight;
       [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
       
       [self.animateView.layer addAnimation:transition forKey:nil];
       [NSTimer scheduledTimerWithTimeInterval:0.6
                                        target:self
                                      selector:@selector(targetMethod:)
                                      userInfo:nil
                                       repeats:NO];
       [self.animateView setHidden:YES];

}

-(void)targetMethod:(NSTimer *)timer
{
    [self.view removeFromSuperview];
}
-(IBAction)btnFbAction:(id)sender
{
    [self.view removeFromSuperview];
//    WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//    create.strComeFrom=@"Fb";
//    [self.controller.navigationController pushViewController:create animated:YES];
}
-(IBAction)btnTwitterAction:(id)sender
{
    [self.view removeFromSuperview];
//    WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//    create.strComeFrom=@"Twitter";
//    [self.controller.navigationController pushViewController:create animated:YES];
}
-(IBAction)btnLinkdAction:(id)sender
{
//    [self.view removeFromSuperview];
//    WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//    create.strComeFrom=@"Youtube";
//    [self.controller.navigationController pushViewController:create animated:YES];
}
-(IBAction)btnYouTubeAction:(id)sender
{
    [self.view removeFromSuperview];
//    WebViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"WebViewController"];
//    create.strComeFrom=@"Youtube";
//    [self.controller.navigationController pushViewController:create animated:YES];
}


@end
