//
//  NewsObject.h
//  CRM
//
//  Created by Arvind Pandey on 4/19/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsObject : NSObject
@property (nonatomic, copy) NSString *FEEDS;
@property (nonatomic, copy) NSString *FEED_DATE;
@property (nonatomic, copy) NSString *newsId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *postDate;
@property (nonatomic, copy) NSString *Description;
@property (nonatomic, copy) NSString *imageurl;


+ (NewsObject *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
@end
