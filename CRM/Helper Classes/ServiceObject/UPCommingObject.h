//
//  UPCommingObject.h
//  CRM
//
//  Created by Sterco Digitex on 23/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UPCommingObject : NSObject
@property (nonatomic, copy) NSString *CITY;
@property (nonatomic, copy) NSString *COUNTRY;
@property (nonatomic, copy) NSString *GAMEID;
@property (nonatomic, copy) NSString *GAME_DATE;
@property (nonatomic, copy) NSString *GAME_INFO;
@property (nonatomic, copy) NSString *GAME_START_DATE;
@property (nonatomic, copy) NSString *GAME_TIME;
@property (nonatomic, copy) NSString *GAME_TYPE;
@property (nonatomic, copy) NSString *SERIESID;
@property (nonatomic, copy) NSString *SERIES_NAME;
@property (nonatomic, copy) NSString *TEAM1;
@property (nonatomic, copy) NSString *TEAM2;
@property (nonatomic, copy) NSString *VENUE;
+ (UPCommingObject *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
@end
