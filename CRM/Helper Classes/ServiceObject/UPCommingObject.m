//
//  UPCommingObject.m
//  CRM
//
//  Created by Sterco Digitex on 23/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "UPCommingObject.h"

@implementation UPCommingObject
+ (UPCommingObject *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    UPCommingObject *instance = [[UPCommingObject alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    
    self.CITY = [aDictionary objectForKey:@"CITY"];
    self.COUNTRY = [aDictionary objectForKey:@"COUNTRY"];
    self.GAMEID = [aDictionary objectForKey:@"GAMEID"];
    self.GAME_DATE = [aDictionary objectForKey:@"GAME_DATE"];
    self.GAME_INFO = [aDictionary objectForKey:@"GAME_INFO"];
    self.GAME_START_DATE = [aDictionary objectForKey:@"GAME_START_DATE"];
    self.GAME_TIME = [aDictionary objectForKey:@"GAME_TIME"];
    self.GAME_TYPE = [aDictionary objectForKey:@"GAME_TYPE"];
     self.SERIESID = [aDictionary objectForKey:@"SERIESID"];
    self.SERIES_NAME = [aDictionary objectForKey:@"SERIESID"];
    self.TEAM1 = [aDictionary objectForKey:@"TEAM1"];
    self.TEAM2 = [aDictionary objectForKey:@"TEAM2"];
    self.VENUE = [aDictionary objectForKey:@"VENUE"];
   
   
  
   
    
    
}

@end
