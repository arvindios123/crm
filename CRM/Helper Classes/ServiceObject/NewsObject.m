//
//  NewsObject.m
//  CRM
//
//  Created by Arvind Pandey on 4/19/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "NewsObject.h"

@implementation NewsObject
+ (NewsObject *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    NewsObject *instance = [[NewsObject alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    
    
    self.newsId = [aDictionary objectForKey:@"NEWSID"];
    self.title = [aDictionary objectForKey:@"NTITLE"];
    self.postDate = [aDictionary objectForKey:@"NDATE"];
    self.postDate = [aDictionary objectForKey:@"NS_DESC"];
    self.imageurl = [aDictionary objectForKey:@"IMAGEFILE"];
    self.FEEDS = [aDictionary objectForKey:@"FEEDS"];
    self.FEED_DATE = [aDictionary objectForKey:@"FEED_DATE"];
    
    
}

@end
