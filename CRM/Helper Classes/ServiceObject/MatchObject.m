//
//  MatchObject.m
//  CRM
//
//  Created by Sterco Digitex on 21/04/18.
//  Copyright © 2018 Aditya Sharma. All rights reserved.
//

#import "MatchObject.h"

@implementation MatchObject
+ (MatchObject *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    MatchObject *instance = [[MatchObject alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    
    
    self.GAMEID = [aDictionary objectForKey:@"GAMEID"];
    self.GAME_TYPE = [aDictionary objectForKey:@"GAME_TYPE"];
    self.GAME_TIME = [aDictionary objectForKey:@"GAME_TIME"];
    self.TEAM1 = [aDictionary objectForKey:@"TEAM1"];
    self.TEAM2 = [aDictionary objectForKey:@"TEAM2"];
    self.VENUE = [aDictionary objectForKey:@"VENUE"];
    self.CITY = [aDictionary objectForKey:@"CITY"];
    self.COUNTRY = [aDictionary objectForKey:@"COUNTRY"];
    self.GAME_INFO = [aDictionary objectForKey:@"GAME_INFO"];
    self.SERIESID = [aDictionary objectForKey:@"SERIESID"];
    
    
}

@end
