//
//  NMGTopView.h
//  MeDoFi
//
//  Created by ARVIND PANDEY on 5/12/16.
//  Copyright © 2016 Arvind Pandey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMGTopView : UIViewController
- (IBAction)clickToSearch:(id)sender;
- (IBAction)click_ToCart:(id)sender;
@property(nonatomic,strong)IBOutlet UILabel *lblCartCount;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UIButton *btnRightMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnCart;
@property(strong,nonatomic)IBOutlet UIImageView *imgCart;
@property(strong,nonatomic)IBOutlet UIImageView *imgSearch;
@property(nonatomic,retain)NSString *str_KeepAction;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIImageView *imgMenu,*imgTitle;

@property BOOL type;
-(id)initWithController:(UIViewController *) controller andViewmenu:(UIView *)view;


@end
