//
//  NMGTopView.m
//  MeDoFi
//
//  Created by ARVIND PANDEY on 5/12/16.
//  Copyright © 2016 Arvind Pandey. All rights reserved.
//

#import "NMGTopView.h"
#import "NMGMoreView.h"
#import "STConfig.h"
//#import "NotificationViewController.h"
//#import "SearchViewController.h"
#import "ViewController.h"

@interface NMGTopView ()
{
  UIStoryboard *mainStoryboard;
}

- (IBAction)clickToGoBack:(id)sender;
@property (strong, nonatomic) NMGMoreView *leftMenu;

@property (nonatomic, retain) NSString *headerButtonTitle;
@property (nonatomic, retain) NSString *heading;
@property (nonatomic, retain) NSString *rightButtonImage;
@property (nonatomic, retain) UIViewController *controller;
@property (nonatomic, retain) UIView *addview;
@property (nonatomic,retain)IBOutlet UIView *back_view;
@property (nonatomic,retain) UIView *viewMenu;

@property BOOL isVisible;
@property BOOL isHidden;


@end

@implementation NMGTopView

-(id)initWithController:(UIViewController *) controller  andViewmenu:(UIView *)view{
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self = [self initWithNibName:@"NMGTopView" bundle:nil];
    self.controller = controller;
    self.viewMenu = view;
    return  self;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
   
    
// Do any additional setup after loading the view from its nib.
}




//Setting for the Label on Header
- (IBAction)clickToHome:(id)sender
{
    if (![self.controller isKindOfClass:[ViewController class]])
    {
        ViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.controller.navigationController pushViewController:create animated:YES];
        
    }
}

- (IBAction)clickToGoBack:(id)sender{
    
    
    self.leftMenu=[[NMGMoreView alloc]initWithController:self.controller];
    [self.leftMenu.view setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    [UIView animateWithDuration:0.1 animations:^void
     {
         
         
     }];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.leftMenu.animateView.layer addAnimation:transition forKey:nil];
    [self.viewMenu addSubview:self.leftMenu.view];
    

    
}





-(void)rightButtonCliked:(id)sender
{
    
}
-(void)leftButtonCliked:(id)sender
{
    NSLog(@"");
}





- (IBAction)clickToSearch:(id)sender
{
    
//    if (![self.controller isKindOfClass:[SearchViewController class]])
//    {
//        SearchViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
//        [self.controller.navigationController pushViewController:create animated:YES];
//
//    }
    
}

- (IBAction)click_ToCart:(id)sender;

{
    
//     if (![self.controller isKindOfClass:[NotificationViewController class]])
//    {
//    NotificationViewController *create = [mainStoryboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
//    [self.controller.navigationController pushViewController:create animated:YES];
//    
//    }
  
}


@end
