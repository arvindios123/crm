//
//  ECSHelper.h
//  TME
//
//  Created by Shreesh Garg on 03/12/13.
//  Copyright (c) 2013 Shreesh Garg. All rights reserved.
//




#import <Foundation/Foundation.h>
#import "ECSHelper.h"




@interface ECSAppHelper : ECSHelper


+(instancetype)sharedInstance;
-(void)presentActionSheetForImageSelection:(UIViewController *)controller;
-(void)startServicemark:(NSString *)type andOrgId:(NSString *)OrgId andStatus:(NSString *)status andController:(UIViewController *)controller;
-(void)startAnimating:(UIImageView *)img;
-(void)stopAnimating;
@end







