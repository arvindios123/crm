


#import "ECSMessage.h"
#import <MessageUI/MFMailComposeViewController.h>
@interface ECSMessage()

@end


@implementation ECSMessage

-(void)sendMailTo:(NSString *)rec bcc:(NSArray *)bccArr cc:(NSArray *)ccArr subject:(NSString *)sub body:(NSString *)body data:(NSData *)da mimeType:(NSString *)mime withController:(UIViewController *)controller
{

    self.parentController=controller;
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = (id<MFMailComposeViewControllerDelegate>)self.parentController;
        
        if(da!=nil)
        [mailComposer addAttachmentData:da mimeType:mime fileName:@"Attachment"];
        [mailComposer setSubject:sub];
        [mailComposer setToRecipients:[NSArray arrayWithObject:rec]];
        if(ccArr!=nil)
        [mailComposer setCcRecipients:ccArr];
        if(bccArr!=nil)
        [mailComposer setBccRecipients:bccArr];
        [mailComposer setMessageBody:body isHTML:YES]; 
        //[controller presentModalViewController:mailComposer animated:YES];
        [self.parentController presentViewController:mailComposer animated:YES completion:nil];
    }
    else
    {
        // Account not configured in iPhone
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Not Configured" message:@"Seems like your iPhone is currently not configured to send emails. Please check your settings and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }

}

-(void)sendMailTo:(NSString *)rec subject:(NSString *)sub body:(NSString *)body withController:(UIViewController *)controller
{

    [self sendMailTo:rec bcc:nil cc:nil subject:sub body:body data:nil mimeType:nil withController:controller];
}


-(void)sendMailTo:(NSString *)rec subject:(NSString *)sub body:(NSString *)body data:(NSData *)da mimeType:(NSString *)mime withController:(UIViewController *)controller
{
    [self sendMailTo:rec bcc:nil cc:nil subject:sub body:body data:da mimeType:mime withController:controller];
}

-(void)sendMailTo:(NSString *)rec bcc:(NSArray *)bccArr subject:(NSString *)sub body:(NSString *)body withController:(UIViewController *)controller
{
    [self sendMailTo:rec bcc:bccArr cc:nil subject:sub body:body data:nil mimeType:nil withController:controller];

}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    // NEVER REACHES THIS PLACE
   [self.parentController dismissViewControllerAnimated:YES completion:NULL];
    NSLog (@"mail finished");
}


//-(void)sendTextMessageTo:(NSString *)recipient body:(NSString *)body  withController:(UIViewController *)controller
//{
//
//    self.parentController=controller;
//    MFMessageComposeViewController *control = [[MFMessageComposeViewController alloc] init];
//    if([MFMessageComposeViewController canSendText])
//    {
//        control.body = body;
//        control.recipients = [NSArray arrayWithObject:recipient];
//        control.messageComposeDelegate = self;
//        [controller presentModalViewController:control animated:YES];
//    }
//}


/*
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self.parentController dismissViewControllerAnimated:YES completion:NULL];
}
*/
@end
