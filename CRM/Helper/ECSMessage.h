


#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface ECSMessage : NSObject <MFMailComposeViewControllerDelegate>
@property (nonatomic, retain)UIViewController *parentController;

//-(void)sendTextMessageTo:(NSString *)recipient body:(NSString *)body  withController:(UIViewController *)controller;


-(void)sendMailTo:(NSString *)rec bcc:(NSArray *)bccArr cc:(NSArray *)ccArr subject:(NSString *)sub body:(NSString *)body data:(NSData *)da mimeType:(NSString *)mime withController:(UIViewController *)controller;

-(void)sendMailTo:(NSString *)rec subject:(NSString *)sub body:(NSString *)body withController:(UIViewController *)controller;


-(void)sendMailTo:(NSString *)rec subject:(NSString *)sub body:(NSString *)body data:(NSData *)da mimeType:(NSString *)mime withController:(UIViewController *)controller;

-(void)sendMailTo:(NSString *)rec bcc:(NSArray *)bccArr subject:(NSString *)sub body:(NSString *)body withController:(UIViewController *)controller;

@end
