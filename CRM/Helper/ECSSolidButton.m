//
//  ECSSolidButton.m
//  TrustYoo
//
//  Created by Shreesh Garg on 26/05/15.
//  Copyright (c) 2015 Shreesh Garg. All rights reserved.
//

#import "ECSSolidButton.h"

@implementation ECSSolidButton

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted)
    {
         self.layer.shadowOpacity = 0.5;
         self.layer.shadowOffset = CGSizeMake(0, 0);
    }
    
    else self.layer.shadowOpacity = 0.0;
}



@end
