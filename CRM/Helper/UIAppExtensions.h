//
//  UIExtensions.h
//  SecureCredentials
//
//  Created by Shreesh Garg on 01/10/13.
//  Copyright (c) 2013 SecureCredentials. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface UIButton  (AppExtensions)

-(void)setButtonBorderColor:(UIColor *)color;

-(void)setFont_H1;
-(void)setFont_H2;
-(void)setFont_H3;
-(void)setFont_H4;
-(void)setFont_H5;
-(void)setFont_H6;
-(void)setFont_H6_White;
-(void)setCornerRadius;


@end



@interface UITextField  (AppExtensions)



@end

//@interface UITextView  (AppExtensions)
//
//-(void)setFont_hint_input_text;
//-(void)setFont_input_text;
//
//- (void)setNumberKeybord;
//
//@end





@interface UILabel  (AppExtensions)

// These name conventions are based on Erika's HTML
-(void)setFont_Gray;
-(void)setFont_Blue;
-(void)setFont_H1_White;
-(void)setFont_H1;
-(void)setFont_H2;
-(void)setFont_H3;
-(void)setFont_H4;
-(void)setFont_H5;
-(void)setFont_H6;
-(void)setFont_H8;
-(void)setFont_H9;
-(void)setFont_WhiteBold;
-(void)setAttributString:(NSString *)string;
-(void)setFirstAttributString:(NSString *)string secondAtrribute:(NSString *)secAttribute andColor:(UIColor *)color;
-(void)setBlankDataMessage:(NSString *)string;
-(void)setCornerRadius;
-(void)makeCircular;
+( void)setFont_H1;

@end

@interface UIFont (AppExtensions)

+(UIFont *)setFont_H1;
+(UIFont *)setFont_H2;
+(UIFont *)setFont_H3;
+(UIFont *)setFont_H4;
+(UIFont *)setFont_H5;
+(UIFont *)setFont_H6;
+(UIFont *)setFont_H8;
+(UIFont *)setFont_H9;
+(UIFont *)setFont_WhiteBold;

@end
@interface UIDatePicker  (AppExtensions)

-(void)setUpDatePickerViewWithDoneButton;
-(void)doneWithNumberPad;
@end








