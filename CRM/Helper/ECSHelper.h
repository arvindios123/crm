//
//  ECSHelper.h
//  TME
//
//  Created by Shreesh Garg on 03/12/13.
//  Copyright (c) 2013 Shreesh Garg. All rights reserved.
//




#import <Foundation/Foundation.h>

@class UserObject;
@class NotificationObject;




@interface ECSHelper : NSObject


//+(void)sendMail:(UIViewController *)controller;
+(void)setRootViewController:(UIViewController *)controller;
+(void)setBlankImageShowdata:(UIViewController *)controller andMessage:(NSString *)message;
+(void)settingRootController:(id)center withMenu:(UIViewController *)menu andCurrentController:(UIViewController *)base;
+(void)attributeStringValue:(NSString *)firstString andSecondAtrribute:(NSString *)strSecoundAttribute;


+(void)clearChache;
+(void)getContectController:(UIViewController *)controller;
@end


@interface ECSAlert : NSObject
+(void)showAlertMessage:(NSString *)massage andController:(UIViewController *)controller goOnTitle:(NSString *) title;
+(void)showAlert:(NSString *)message;
+(void)showApiError:(NSDictionary *)rootDictionary respString:(NSString *)respString error:(NSError *)error;
+(void)showAlert:(NSString *)message withDelegate:(UIViewController *)ctrl andTag:(int)tag;
+(void)showAlertView:(NSString *)massage WithDelegate:(UIViewController *)controller andTag:(int)tag cancelTitle:(NSString *)clrButton goOnTitle:(NSString *)gotitle action:(SEL) sel;

@end




@interface ECSUserDefault: NSObject

+(BOOL)getBoolFromUserDefaultForKey:(NSString *)string;
+(NSInteger)getIntFromUserDefaultForKey:(NSString *)string;
+(NSString *)getStringFromUserDefaultForKey:(NSString *)string;
+(float)getFloatFromUserDefaultForKey:(NSString *)string;
+(id)getObjectFromUserDefaultForKey:(NSString *)string;


+(void)saveBool:(BOOL)val ToUserDefaultForKey:(NSString *)string;
+(void)saveInt:(int)val ToUserDefaultForKey:(NSString *)string;
+(void)saveString:(NSString *)val ToUserDefaultForKey:(NSString *)string;
+(void)saveFloat :(float)val ToUserDefaultForKey:(NSString *)string;
+(void)saveObject:(id)val ToUserDefaultForKey:(NSString *)string;

@end



@interface ECSDate : NSObject

+(NSDate *)dateAfterYear:(int)y month:(int)m day:(int)d fromDate:(NSDate *)date;
+(NSString *)getFormattedDateString:(NSDate *)date;
+(NSString *)getStringFromDate:(NSDate *)date inFormat:(NSString *)formString;
+(NSString *)getTheTimeDiffrenceString:(NSString *)createdAt;
+(NSString *)getTheTimeDiffrenceStringForComment:(NSString *)createdAt;
+(NSString *)getCurrentDateAndTime;
+(NSString *)getTimeFromTimeStemp:(NSString *)timeStemp;
+(NSString *)getDateFromTimeStemp:(NSString *)timeStemp;

@end


@interface ECSSearch : NSObject
+(NSMutableArray *)managingDisplayArrayFromArray:(NSMutableArray *)totalArray withKey:(NSString *)key;
@end


@interface ECSDevice : NSObject

+(BOOL)isiOS8;
+(BOOL)isiOS7;
+(BOOL)isiOS6;
+(BOOL)isiOS5;
+(float)iOSVersion;
@end





