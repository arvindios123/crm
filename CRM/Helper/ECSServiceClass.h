//
//  ECSServiceClass.h
//  Ag Mobile App
//
//  Created by Shreesh Garg on 03/03/13.
//  Copyright (c) 2013 eComStreet. All rights reserved.
//

#define CONTENT_TYPE_VAL       @"application/x-www-form-urlencoded"
#define CONTENT_TYPE_KEY       @"Content-Type"
//#define SERVERURLPATH          @"http://newmediaguru.co/demo/medofi/api/services"
#define SERVERURLPATH          @"http://stagging.cricketmazza.com"

typedef enum
{
    GET =0,POST = 1, PUT = 2, DELETE = 3, IMAGE = 4, IMAGEPUT = 5

}ServiceMethod;


#import <Foundation/Foundation.h>


@interface ECSResponse : NSObject



@property (nonatomic, retain) NSData *data;
@property (nonatomic, retain) NSError *error;
@property int statusCode;
@property BOOL isValid;

@property (nonatomic, retain) NSDictionary * extraDictionary;
@property (nonatomic, retain) NSMutableURLRequest *urlRequest;
@property (nonatomic, retain) NSString *stringValue;

@end

@interface ECSServiceClass : NSObject

@property ServiceMethod serviceMethod;
@property SEL callback;
@property BOOL isTracking;
@property (nonatomic, retain) UIViewController *controller;
@property (nonatomic, retain) NSString *serviceURL;


-(void)addParam:(NSString *)value forKey:(NSString *)key;
-(void)addHeader:(NSString *)value forKey:(NSString *)key;
-(void)addImageData:(NSData *)data withName:(NSString *)name forKey:(NSString *)key;
-(void)addExtraParam:(NSString *)value forKey:(NSString *)key;
//-(void)runGetService;
//-(void)runPostService;
-(void)runService;
-(void)cancelConnection;






@end
