//
//  ECSHelper.m
//  TME
//
//  Created by Shreesh Garg on 03/12/13.
//  Copyright (c) 2013 Shreesh Garg. All rights reserved.
//

#import "ECSConfig.h"
#import "ECSAppHelper.h"
#import "UIExtensions.h"
#import <AVFoundation/AVFoundation.h>
#import "UIAppExtensions.h"


static ECSAppHelper * appHelper;


@interface ECSAppHelper ()<UIActionSheetDelegate>
@property (strong, retain) UIViewController * controller;
@end

@implementation ECSAppHelper


+(instancetype)sharedInstance
{
   if(appHelper == nil)
   {
       appHelper = [[ECSAppHelper alloc]init];
   }
  return appHelper;
}


-(void)presentActionSheetForImageSelection:(UIViewController *)controller
{
    [ECSAppHelper sharedInstance].controller = controller;
    UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Library",nil];
    [actionSheet showInView:controller.view];
}


// delegate method of UIAction sheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if(buttonIndex == 2) return;
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if(buttonIndex == 0)
    {
        
        NSError *error=nil;
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
        if(deviceInput)
           [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        else {
        
            [ECSAlert showAlert:@"You have not given permission to TY to use camera. You can change you privacy setting in iPhone's Settings."];
            return;
        
        }
       
    }
    else if(buttonIndex == 1)
    {
        imagePicker.allowsEditing = NO;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [imagePicker setDelegate:[ECSAppHelper sharedInstance].controller];
    [[ECSAppHelper sharedInstance].controller presentViewController:imagePicker animated:YES completion:nil];
    
}

-(void)startAnimating:(UIImageView *)img
{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 30)];
    [spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    spinner.color = [UIColor blueColor];
    [spinner startAnimating];
    [img addSubview:spinner];
}




@end







