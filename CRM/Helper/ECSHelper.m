//
//  ECSHelper.m
//  TME
//
//  Created by Shreesh Garg on 03/12/13.
//  Copyright (c) 2013 Shreesh Garg. All rights reserved.
//
#define APPNAME @"MeDoFi"
#import "ECSConfig.h"
#import "ECSHelper.h"
#import "UIExtensions.h"
#import <MessageUI/MessageUI.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import "ECSConfig.h"
#import "AppDelegate.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>


@interface ECSHelper ()<MFMailComposeViewControllerDelegate,ABPeoplePickerNavigationControllerDelegate>
@property (nonatomic, retain) UIViewController * controller;
@end

@implementation ECSHelper

//+(void)sendMail:(UIViewController *)controller
//{
//    
//    if ([MFMailComposeViewController canSendMail])
//    {
//        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
//        mail.mailComposeDelegate = self;
//        [mail setSubject:@"Sample Subject"];
//        [mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];
//        [mail setToRecipients:@[@"testingEmail@example.com"]];
//    
//        [controller presentViewController:mail animated:YES completion:NULL];
//    }
//    else
//    {
//        NSLog(@"This device cannot send email");
//    }
//}
//
//- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
//{
//    switch (result) {
//        case MFMailComposeResultSent:
//            NSLog(@"You sent the email.");
//            break;
//        case MFMailComposeResultSaved:
//            NSLog(@"You saved a draft of this email");
//            break;
//        case MFMailComposeResultCancelled:
//            NSLog(@"You cancelled sending this email.");
//            break;
//        case MFMailComposeResultFailed:
//            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
//            break;
//        default:
//            NSLog(@"An error occurred when trying to compose this email");
//            break;
//    }
//    
//    [controller dismissViewControllerAnimated:YES completion:NULL];
//}
+(void)setRootViewController:(UIViewController *)controller
{
//    AppDelegate * appDelegate = [[UIApplication sharedApplication]delegate];
//    NSMutableArray * controllersArray = [[[appDelegate navigationController] viewControllers]mutableCopy];
//    [controllersArray removeAllObjects];
//    [controllersArray addObject:controller];
//    [[appDelegate navigationController]setViewControllers:controllersArray];
}



+(void)getCurrentLocation:(NSString *)latLong withController:(UIViewController *)controller
{
    
}

+(void)attributeStringValue:(NSString *)firstString andSecondAtrribute:(NSString *)strSecoundAttribute
{
}
+(void)getContectController:(UIViewController *)controller
{
    
   
    NSArray *thePeopleList = nil;
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, nil);
        thePeopleList = (__bridge_transfer NSArray*)ABAddressBookCopyArrayOfAllPeople(addressBook);
        
    }
    
    NSMutableDictionary * rootDictionary = [[NSMutableDictionary alloc]init];
    NSMutableArray * contactArray = [[NSMutableArray alloc]init];
    
    for(int iTotal = 0 ; iTotal < thePeopleList.count ; iTotal ++)
    {
        
        NSMutableDictionary * contactDictionary = [[NSMutableDictionary alloc]init];
        
        
        ABRecordRef person = CFArrayGetValueAtIndex( (__bridge CFArrayRef)(thePeopleList), iTotal );
        
        ABMultiValueRef phonesRef = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        ABMultiValueRef emailsRef = ABRecordCopyValue(person, kABPersonEmailProperty);
        
        NSString * firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
        NSString * lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
        
        
        if((ABMultiValueGetCount(emailsRef) > 0 || ABMultiValueGetCount(phonesRef) > 0) && (lastName != NULL || firstName != NULL))
        {
            CFStringRef currentEmailValue = ABMultiValueCopyValueAtIndex(emailsRef, 0);
            NSString * email = (__bridge NSString *)currentEmailValue;
            CFStringRef currentPhoneValue = ABMultiValueCopyValueAtIndex(phonesRef, 0);
            NSString * phoneNo = (__bridge NSString *)currentPhoneValue;
            if(email != NULL)
            {
               email =[email stringByReplacingOccurrencesOfString:@"&" withString:@" "];
                [contactDictionary setObject:email forKey:@"email"];
            }
            
            CFRelease(emailsRef);
            if(phoneNo != NULL)
            {
                phoneNo =[phoneNo stringByReplacingOccurrencesOfString:@"&" withString:@" "];
                [contactDictionary setObject:phoneNo forKey:@"phone_no"];
            }
            
            CFRelease(phonesRef);
            
            if(firstName != NULL)
            {
                firstName =[firstName stringByReplacingOccurrencesOfString:@"&" withString:@" "];
                [contactDictionary setObject:firstName forKey:@"first_name"];
            }
            
            if(lastName != NULL)
            {
                lastName =[lastName stringByReplacingOccurrencesOfString:@"&" withString:@" "];
                [contactDictionary setObject:lastName forKey:@"last_name"];
            }
                      [contactArray addObject:contactDictionary];
        }
        
        
        
    }
    
    [rootDictionary setObject:contactArray forKey:@"contacts"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rootDictionary options:0 error:&error];
    
    NSString *jsonString = nil;
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    [controller performSelector:@selector(getUserContectData:) withObject:jsonString];
    
}




+(void)clearChache
{
    NSError * error = nil;
    NSFileManager * fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[NSHomeDirectory() stringByAppendingString:@"/Library/Caches/"] error:&error];
    // NSLog(@"error is memory is %@",error.localizedDescription);
}
@end


@implementation ECSAlert
+(void)showAlertMessage:(NSString *)massage andController:(UIViewController *)controller goOnTitle:(NSString *) title
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:massage preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
         [controller.navigationController popViewControllerAnimated:NO];
        //[controller performSelector:@selector(closeAlertViewM:) withObject:nil];
       // [self closeAlertview];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [controller presentViewController:alertController animated:YES completion:nil];
    });
}
-(void)closeAlertViewM:(UIViewController *)controller
{
    [controller.navigationController popViewControllerAnimated:NO];
}
+(void)showAlert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAppName message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];


}

+(void)showApiError:(NSDictionary *)rootDictionary respString:(NSString *)respString error:(NSError *)error
{
    if(respString.length == 0)
    {
       [ECSAlert showAlert:error.localizedDescription];
    }
    else
    {
    NSArray * errorArray = [rootDictionary objectForKey:@"error"];
    if((errorArray) && [errorArray isKindOfClass:[NSArray class]])
    {
        if(errorArray.count > 0)
        {
        NSString * erroString = [NSString stringWithFormat:@"ERROR_%@",[errorArray objectAtIndex:0]];
        [ECSAlert showAlert:erroString];
        }
        else [ECSAlert showAlert:respString];
    }
    else if([errorArray isKindOfClass:[NSString class]])
    {
         NSString * errorMessage = (NSString *)errorArray;
         [ECSAlert showAlert:errorMessage];
    }
    else [ECSAlert showAlert:respString];
    }
}


+(void)showAlert:(NSString *)message withDelegate:(UIViewController *)ctrl andTag:(int)tag
{

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAppName message:message delegate:ctrl cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert setTag:tag];
    [alert show];

}
+(void)showAlertView:(NSString *)massage WithDelegate:(UIViewController *)controller andTag:(int)tag cancelTitle:(NSString *)clrButton goOnTitle:(NSString *)gotitle action:(SEL) sel
{
    
    if([ECSDevice iOSVersion] >= 8)
    {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:kAppName message:massage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:gotitle style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       
                                                       [controller performSelector:sel withObject:nil];
                                                       
                                                       
                                                   }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:clrButton style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           //NSLog(@"cancel btn");
                                                           
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                           
                                                       }];
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [controller presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAppName message:massage delegate:controller cancelButtonTitle:clrButton otherButtonTitles:gotitle, nil];
        [alert setTag:tag];
        [alert show];
    }
}


@end


@implementation ECSUserDefault


// get the values from user defaults

+(BOOL)getBoolFromUserDefaultForKey:(NSString *)string
{
    return [[NSUserDefaults standardUserDefaults]boolForKey:string];

}

+(NSInteger)getIntFromUserDefaultForKey:(NSString *)string{
    
    return [[NSUserDefaults standardUserDefaults]integerForKey:string];

}
+(NSString *)getStringFromUserDefaultForKey:(NSString *)string{

    return [[NSUserDefaults standardUserDefaults]stringForKey:string];

}
+(float)getFloatFromUserDefaultForKey:(NSString *)string{
 
    return [[NSUserDefaults standardUserDefaults]floatForKey:string];

}
+(id)getObjectFromUserDefaultForKey:(NSString *)string{

   return [[NSUserDefaults standardUserDefaults]objectForKey:string];
    
}

// svae the values to user defaults 


+(void)saveBool:(BOOL)val ToUserDefaultForKey:(NSString *)string{
    
    [[NSUserDefaults standardUserDefaults]setBool:val forKey:string];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(void)saveInt:(int)val ToUserDefaultForKey:(NSString *)string
{
    [[NSUserDefaults standardUserDefaults]setInteger:val forKey:string];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


+(void)saveString:(NSString *)val ToUserDefaultForKey:(NSString *)string
{
    [[NSUserDefaults standardUserDefaults]setObject:val forKey:string];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


+(void)saveFloat :(float)val ToUserDefaultForKey:(NSString *)string
{
    [[NSUserDefaults standardUserDefaults]setFloat:val forKey:string];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


+(void)saveObject:(id)val ToUserDefaultForKey:(NSString *)string
{
    [[NSUserDefaults standardUserDefaults]setObject:val forKey:string];
    [[NSUserDefaults standardUserDefaults]synchronize];
}





@end


// Implementation of ecsdate helper



@implementation ECSDate
+(NSString *)getTimeFromTimeStemp:(NSString *)timeStemp;
{
    
    double timeStamp = [timeStemp intValue];
    NSTimeInterval timeInterval=timeStamp ;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"hh:mm a"];
    //[dateformatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *dateString=[dateformatter stringFromDate:date];
    return dateString;
}

+(NSString *)getDateFromTimeStemp:(NSString *)timeStemp;
{
   
    double timeStamp = [timeStemp intValue];
    NSTimeInterval timeInterval=timeStamp ;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    //[dateformatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *dateString=[dateformatter stringFromDate:date];
    return dateString;
}


+(NSString *)getTheTimeDiffrenceString:(NSString *)createdAt
{
    NSString * string = nil;
    int value = 0;
    NSString * unit = @"";
    
    NSDate * parsedDate = [NSDate dateWithTimeIntervalSince1970:[createdAt doubleValue]];

    int epochTime = (int)[[NSDate date] timeIntervalSinceDate:parsedDate];
    
    if(epochTime < 60)
    {
        string = [NSString stringWithFormat:@"%is ago",epochTime];
    }
    else if (epochTime < 60*60)
    {
        value = epochTime/60;
        if(value == 1) unit = @"min";
        else unit = @"mins";
        string = [NSString stringWithFormat:@"%i %@ ago",value,unit];
    }
    else if (epochTime < 60 * 60 * 24)
    {
        value = epochTime/(60 * 60);
        if(value == 1) unit = @"hour";
        else unit = @"hr";
        string = [NSString stringWithFormat:@"%i %@ ago",value,unit];
    }
    else if (epochTime < 60 * 60 * 24 * 30)
    {
        value = epochTime/(60 * 60 * 24);
        if(value == 1) unit = @"day";
        else unit = @"days";
        string = [NSString stringWithFormat:@"%i %@ ago",value,unit];
    }
    else if (epochTime < 60 * 60 * 24 * 30 * 12)
    {
        value = epochTime/(60 * 60 * 24 * 30);
        if(value == 1) unit = @"month";
        else unit = @"months";
        string = [NSString stringWithFormat:@"%i %@ ago",value,unit];
    }
    else
    {
        int epochYear = epochTime / (60 * 60 * 24 * 30 * 12);
        int monthSecond = (int)epochTime % epochYear;
        int epochMonth = monthSecond / (60 * 60 * 24 * 30 * 12);
//        NSString * yearString = ;
//        NSString * monthString = @"months";
        NSString * yearString = epochYear== 1?@"year":@"years";
        NSString * monthString = epochMonth== 1?@"month":@"months";
        
        if(epochMonth > 0)
        {
            
            string = [NSString stringWithFormat:@"%i %@ %i %@ ago",epochYear,yearString,epochMonth,monthString];
        }
        else
            string = [NSString stringWithFormat:@"%i %@ ago",epochYear,yearString];
        
    }
    return string;
}



+(NSString *)getTheTimeDiffrenceStringForComment:(NSString *)createdAt
{
    NSString * string = nil;
    int value = 0;
    NSString * unit = @"";
    
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate * parsedDate = [formatter dateFromString:createdAt];
    
    //secondsFromGMTForDate
    int epochTime = (int)[[NSDate date] timeIntervalSinceDate:parsedDate];
    
    if(epochTime < 60)
    {
        string = [NSString stringWithFormat:@"%is",epochTime];
    }
    else if (epochTime < 60*60)
    {
        value = epochTime/60;
        string = [NSString stringWithFormat:@"%i m",value];
    }
    else if (epochTime < 60 * 60 * 24)
    {
        value = epochTime/(60 * 60);
        string = [NSString stringWithFormat:@"%i h",value];
    }
    else if (epochTime < 60 * 60 * 24 * 30)
    {
        value = epochTime/(60 * 60 * 24);
        string = [NSString stringWithFormat:@"%i d",value];
    }
    else if (epochTime < 60 * 60 * 24 * 30 * 12)
    {
        value = epochTime/(60 * 60 * 24 * 30);
        if(value == 1) unit = @"month";
        else unit = @"months";
        string = [NSString stringWithFormat:@"%i %@",value,unit];
    }
    else
    {
        int epochYear = epochTime / (60 * 60 * 24 * 30 * 12);
        int monthSecond = (int)epochTime % epochYear;
        int epochMonth = monthSecond / (60 * 60 * 24 * 30 * 12);
        //        NSString * yearString = ;
        //        NSString * monthString = @"months";
        NSString * yearString = epochYear== 1?@"year":@"years";
        NSString * monthString = epochMonth== 1?@"month":@"months";
        
        if(epochMonth > 0)
        {
            
            string = [NSString stringWithFormat:@"%i %@ %i %@",epochYear,yearString,epochMonth,monthString];
        }
        else
            string = [NSString stringWithFormat:@"%i %@",epochYear,yearString];
        
    }
    return string;
}


+(NSDate *)dateAfterYear:(int)y month:(int)m day:(int)d fromDate:(NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setYear:y];
    [offsetComponents setMonth:m];
    [offsetComponents setDay:d];
    NSDate *calculatedDate = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
    return calculatedDate;
    
    
}

+(NSString *)getFormattedDateString:(NSDate *)date
{
     NSDateFormatter *formatter = nil;
     formatter = [[NSDateFormatter alloc]init];
     [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [formatter setTimeZone:timeZone];
     NSString *string = [formatter stringFromDate:date];formatter = nil;
     return string;
}


+(NSString *)getStringFromDate:(NSDate *)date inFormat:(NSString *)formString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:formString];
    NSString *string = [formatter stringFromDate:date];
    return string;
}

@end





@implementation ECSSearch

//+(NSMutableArray *)managingDisplayArrayFromArray:(NSMutableArray *)totalArray withKey:(NSString *)key{
//    
//    if(key.length == 0)
//        return [NSMutableArray arrayWithArray:totalArray];
//    NSMutableArray *ary=[[NSMutableArray alloc]init];
//    key=[key lowercaseString];
//    for (UserObject * tagObject in totalArray)
//    {
//        NSRange range = [[NSString stringWithFormat:@"%@ %@",[tagObject.firstName lowercaseString],[tagObject.lastName lowercaseString]] rangeOfString:key];
//        if (range.location != NSNotFound )
//        {
//            tagObject.isSelected = YES;
//            [ary addObject:tagObject];
//        }
//
//    }
//    return ary;
//
//}

@end


@implementation ECSDevice

+(BOOL)isiOS8
{
    if([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)
        return YES;
    else return NO;
}

+(BOOL)isiOS7
{
  if([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
      return YES;
  else return NO;
}
+(BOOL)isiOS6
{
    if([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)
        return YES;
    else return NO;
}
+(BOOL)isiOS5
{
    if([[UIDevice currentDevice].systemVersion floatValue] >= 5.0)
        return YES;
    else return NO;
}
+(float)iOSVersion
{
    return [[UIDevice currentDevice].systemVersion floatValue];
}
@end







