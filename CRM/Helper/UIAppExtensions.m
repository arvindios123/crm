//
//  UIExtensions.m
//  SecureCredentials
//
//  Created by Shreesh Garg on 01/10/13.
//  Copyright (c) 2013 SecureCredentials. All rights reserved.
//

#import "UIAppExtensions.h"
#import "UIExtensions.h"

#import <QuartzCore/QuartzCore.h>




@implementation UIButton  (AppExtensions)

-(void)setButtonBorderColor:(UIColor *)color
{
    [self.layer setBorderColor:color.CGColor];
}

-(void)setFont_H1
{
    
    [self setButtonTitleColor:[UIColor whiteColor]];
    [self.titleLabel setFont:[UIFont setFont_H1]];
    
    
}
-(void)setFont_H2
{
    [self setButtonTitleColor:[UIColor whiteColor]];
    [self.titleLabel setFont:[UIFont setFont_H2]];
}
-(void)setFont_H8
{
    [self setButtonTitleColor:[UIColor colorWithR:153 G:153 B:153]];
    [self.titleLabel setFont:[UIFont setFont_H8]];
}
-(void)setFont_H3
{
    [self setButtonTitleColor:[UIColor colorWithR:51 G:51 B:51]];
    [self.titleLabel setFont:[UIFont setFont_H3]];
}

-(void)setFont_H4
{
    [self setButtonTitleColor:[UIColor colorWithR:51 G:51 B:51]];
    [self.titleLabel setFont:[UIFont setFont_H4]];
    
}
-(void)setFont_H5
{
    [self setButtonTitleColor:[UIColor whiteColor]];
    [self.titleLabel setFont:[UIFont setFont_H5]];
}
-(void)setFont_H6
{
    [self setButtonTitleColor:[UIColor colorWithR:102 G:102 B:102]];
    [self.titleLabel setFont:[UIFont setFont_H6]];
    
}
-(void)setFont_H6_White
{
    [self setButtonTitleColor:[UIColor whiteColor]];
    [self.titleLabel setFont:[UIFont setFont_H6]];
    
}
-(void)setCornerRadius
{
    self.layer.masksToBounds=YES;
    
    self.layer.cornerRadius =5.0;
}


@end




@implementation UITextField  (AppExtensions)




@end











@implementation UILabel (AppExtensions)
-(void)setFont_Blue
{
    [self setTextColor:[UIColor colorWithR:20 G:116 B:193]];
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setFont:[UIFont setFont_H1]];
}

-(void)setFont_Gray
{
    [self setTextColor:[UIColor colorWithR:159 G:159 B:164]];
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setFont:[UIFont setFont_H1]];

}

-(void)makeCircular
{
    self.layer.cornerRadius = self.frame.size.width/2;
    [self.layer setMasksToBounds:YES];
    
}
-(void)setFont_H9
{
    
    [self setTextColor:[UIColor colorWithR:107 G:105 B:107]];
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setFont:[UIFont setFont_H9]];
    
    
}
-(void)setFont_H1
{

    [self setTextColor:[UIColor redColor]];
    [self setFont:[UIFont setFont_H1]];


}

-(void)setFont_H1_White
{
    
    [self setTextColor:[UIColor whiteColor]];
    [self setFont:[UIFont setFont_H1]];
    
    
}

-(void)setFont_H2
{
    [self setTextColor:[UIColor colorWithR:153 G:153 B:153]];
    [self setFont:[UIFont setFont_H2]];
}
-(void)setFont_H3
{
    [self setTextColor:[UIColor colorWithR:85 G:85 B:85]];
    [self setFont:[UIFont setFont_H3]];
}

-(void)setFont_H4
{
    [self setTextColor:[UIColor colorWithR:51 G:51 B:51]];
    [self setFont:[UIFont setFont_H4]];
}
-(void)setFont_H5
{
    [self setTextColor:[UIColor redColor]];
    [self setFont:[UIFont setFont_H5]];
}
-(void)setFont_H6
{
    [self setTextColor:[UIColor colorWithR:102 G:102 B:102]];
    [self setFont:[UIFont setFont_H6]];
}
-(void)setFont_WhiteBold;
{
    [self setTextColor:[UIColor colorWithR:255 G:255 B:255]];
    [self setFont:[UIFont setFont_WhiteBold]];
}

-(void)setCornerRadius
{
    self.layer.masksToBounds=YES;
  
  self.layer.cornerRadius =5.0;
}

-(void)setAttributString:(NSString *)string
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",string]];
    //14,119,161
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor colorWithR:14 G:119 B:61]
                             range:NSMakeRange(0, 1)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Arial" size:20] range:NSMakeRange(0, 1)];
    [self setAttributedText:attributedString];
    
}

-(void)setFirstAttributString:(NSString *)string secondAtrribute:(NSString *)secAttribute andColor:(UIColor *)color
{
    
    NSString *strCaption = [NSString stringWithFormat:@"%@ %@",string,secAttribute];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:strCaption];
    [attString addAttribute:NSForegroundColorAttributeName value:color range:[strCaption rangeOfString:secAttribute]];
    [self setNumberOfLines:2];
   [self setAttributedText:attString];
  
    
}
-(void)setBlankDataMessage:(NSString *)string
{
    [self setTextColor:[UIColor colorWithR:255 G:255 B:255]];
    [self setFont:[UIFont setFont_WhiteBold]];
}
@end





@implementation UIView (Extensions)



@end



@implementation UIFont (AppExtensions)



+(UIFont *)setFont_H9
{
    return [UIFont fontWithName:@"Helvetica Neue" size:17.0f];
}

+(UIFont *)setFont_H1
{
    return [UIFont fontWithName:@"Helvetica-Bold" size:15.0f];
}
+(UIFont *)setFont_H2
{
    return [UIFont fontWithName:@"Helvetica-Bold" size:13.0f];
}
+(UIFont *)setFont_H8
{
    return [UIFont fontWithName:@"Arial-BoldMT" size:8.0f];
}

+(UIFont *)setFont_H3{
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f];
    
}
+(UIFont *)setFont_H4{
     return [UIFont fontWithName:@"Helvetica-Bold" size:25.0f];
}
+(UIFont *)setFont_H5{
    
     return [UIFont fontWithName:@"Helvetica-Bold" size:17.0f];
    
}
+(UIFont *)setFont_H6
{
    
 return [UIFont fontWithName:@"Helvetica Neue" size:17.0f];
    
}
+(UIFont *)setFont_WhiteBold;
{
    
    return [UIFont fontWithName:@"Arial-BoldMT" size:12.0f];
    
}
@end
@implementation UIDatePicker (AppExtensions)

-(void)setUpDatePickerViewWithDoneButton
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    [self addSubview:numberToolbar];
    
}
-(void)doneWithNumberPad{
    
    [self.superview removeFromSuperview];
}
@end



